import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Window 2.3
import QtMultimedia 5.9
import QtGraphicalEffects 1.0

ApplicationWindow {
    visible: true
    width: 480
    height: 720
    title: qsTr("Tabs")
    minimumWidth: 480
    minimumHeight: 720
    maximumHeight: minimumHeight
    maximumWidth: minimumWidth
    readonly property int dpi: Screen.pixelDensity * 25.4
    function dp(x){ return (dpi < 120) ? x : x*(dpi/160); }
    property var aval: false
    header:
        //Application Bar
        Rectangle {
        id: menuRect
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        height: dp(48)
        color: "green"
        Text{
            id: textbar
            text:
            {
                if(swipeView.currentIndex == 0){qsTr("Авторизация")}
                if(swipeView.currentIndex == 1){qsTr("Пользователь: " + edtLogin1.text)}
                if(swipeView.currentIndex == 2){qsTr("Информация")}
            }
            font.bold: true;
            font.pixelSize: 20
            color: "white"
            anchors{
                verticalCenter: parent.verticalCenter
                horizontalCenter: parent.horizontalCenter
            }
        }
        Rectangle {
            id: navPanel
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            visible: false
            width: dp(48)
            color: "green"
            Rectangle {
                anchors.top: parent.top
                anchors.topMargin: dp(16)
                anchors.left: parent.left
                anchors.leftMargin: dp(14)
                width: dp(20)
                height: dp(2)
            }

            Rectangle {
                anchors.top: parent.top
                anchors.topMargin: dp(23)
                anchors.left: parent.left
                anchors.leftMargin: dp(14)
                width: dp(20)
                height: dp(2)
            }

            Rectangle {
                anchors.top: parent.top
                anchors.topMargin: dp(30)
                anchors.left: parent.left
                anchors.leftMargin: dp(14)
                width: dp(20)
                height: dp(2)
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    nav.toggle()
                }
            }
        }
    }


    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex
        interactive: false
        Page {
                ColumnLayout {
                    clip: false
                    opacity: 0.8
                    anchors.leftMargin: 0
                    anchors.fill: parent;
                    Rectangle {
                        Layout.preferredWidth:  70 * Screen.pixelDensity
                        Layout.preferredHeight: 40 * Screen.pixelDensity
                        Layout.alignment: Qt.AlignCenter
                        ColumnLayout {
                            anchors.centerIn: parent;
                            width:50 * Screen.pixelDensity
                            height:50 * Screen.pixelDensity
                            Rectangle {
                                color: "white"
                                border.width: 1
                                border.color: "black"
                                Layout.preferredWidth:  40 * Screen.pixelDensity
                                Layout.preferredHeight: 10 * Screen.pixelDensity
                                Layout.alignment: Qt.AlignCenter
                                TextField {
                                    id: edtLogin1
                                    font.pointSize: 10
                                    anchors.fill: parent;
                                    placeholderText: "Enter your login"
                                    //при размещении в лэйоут элементов,  мы не используем width и x,y
                                }
                            }
                            Rectangle {
                                color: "white"
                                border.width: 1
                                border.color: "black"
                                Layout.preferredWidth:  40 * Screen.pixelDensity
                                Layout.preferredHeight: 10 * Screen.pixelDensity
                                Layout.alignment: Qt.AlignCenter
                                TextField {
                                    id: edtPassword1
                                    font.pointSize: 10
                                    anchors.fill: parent;
                                    echoMode: "Password"
                                    placeholderText: "Enter your password"
                                    //при размещении в лэйоут элементов,  мы не используем width и x,y
                                }
                            }
                            Text {
                                id: errorText
                                visible: false
                                color: "red"
                                font.bold: true
                                font.pointSize: 10
                                text: "Неверный логин или пароль"
                                Layout.alignment: Qt.AlignCenter
                            }

                            DropShadow {
                                anchors.fill: btnAuth
                                horizontalOffset: 3
                                verticalOffset: 3
                                radius: 8.0
                                samples: 17
                                color: "#80000000"
                                source: btnAuth
                            }
                            Button {
                                id: btnAuth
                                text: "Вход"
                                font.pointSize: 18
                                Layout.preferredWidth:  40 * Screen.pixelDensity
                                Layout.preferredHeight: 10 * Screen.pixelDensity
                                Layout.alignment: Qt.AlignCenter
                                onClicked: {
                                    if (edtLogin1.text == "admin" && edtPassword1.text == "admin") {
                                        nav.visible = true
                                        navPanel.visible = true
                                        errorText.visible = false
                                        aval = true
                                        tabBar.visible = true
                                        edtPassword1.clear()
                                        swipeView.interactive = true
                                        swipeView.setCurrentIndex(swipeView.currentIndex + 1)
                                    } else {
                                        errorText.visible = true
                                        edtLogin1.clear()
                                        edtPassword1.clear()
                                    }
                                }
                            }
                        }
                   }
            }
        }

        Page {
                ColumnLayout {
                    clip: false
                    opacity: 0.8
                    anchors.leftMargin: 0
                    anchors.fill: parent;

                    Button {
                        id: btnReq
                        text: "Запрос"
                        font.pointSize: 18
                        Layout.row: 1
                        Layout.preferredWidth:  40 * Screen.pixelDensity
                        Layout.preferredHeight: 10 * Screen.pixelDensity
                        Layout.alignment: Qt.AlignCenter
                    }
                    DropShadow {
                        anchors.fill: btnReq
                        horizontalOffset: 3
                        verticalOffset: 3
                        radius: 8.0
                        samples: 17
                        color: "#80000000"
                        source: btnReq
                    }
                }
        }

        Page {
                ColumnLayout {
                    clip: false
                    opacity: 0.8
                    anchors.leftMargin: 0
                    anchors.fill: parent


                    Rectangle {
                        id: logo
                        Layout.preferredWidth:  80 * Screen.pixelDensity
                        Layout.preferredHeight: 20 * Screen.pixelDensity
                        Layout.row: 1
                        Layout.alignment: Qt.AlignCenter
                        Image {
                            id: mosPol
                            anchors.fill: parent
                            source: "./MosPol.png"
                        }
                    }

                    Text {
                        font.pointSize: 8
                        Layout.row: 2

                        anchors.horizontalCenter: parent.horizontalCenter
                        text: "Программа на вторую пересдачу дисциплины \n\"Программирование безопасных мобильных приложений\""
                    }

                }
        }

    }


    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex
        visible: false
        background: Rectangle {
                color: "green"
                anchors.fill: parent
        }
        TabButton {
            text: qsTr("Авторизация")
            background: Rectangle {
                color: tabBar.currentIndex == 0 ? "white" : "green"
            }
        }
        TabButton {
            text: qsTr("Запрос")
            background: Rectangle {
                color: tabBar.currentIndex == 1 ? "white" : "green"
            }
        }
        TabButton {
            text: qsTr("Информация")
            background: Rectangle {
                color: tabBar.currentIndex == 2 ? "white" : "green"
            }
        }
    }



    NavigationDrawer {
        id: nav
        anchors.bottom: parent.bottom
        visible: false
        position: Qt.LeftEdge
        //visualParent: stackView
        Rectangle {
            anchors.fill: parent
            color: "green"
            ListView{
                id: lstPagesMenu
                model: navModel
                anchors.fill: parent
                anchors.margins: 10
                delegate: Button
                {
                    text: stranica
                    width: lstPagesMenu.width
                    height:Screen.pixelDensity * 12
                    onClicked:
                    {
                        swipeView.currentIndex = index
                    }
                }
            }
        }
    }
    ListModel {
        id: navModel

        ListElement {stranica: "Авторизация"}
        ListElement {stranica: "Запрос"}
        ListElement {stranica: "Информация"}
    }
}
